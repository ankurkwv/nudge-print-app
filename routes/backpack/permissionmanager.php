<?php

/*
|--------------------------------------------------------------------------
| Backpack\PermissionManager Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are
| handled by the Backpack\PermissionManager package.
|
*/

Route::group([
	    'prefix'     => config('backpack.base.route_prefix', 'admin'),
	    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
	    'namespace'  => 'App\Http\Controllers\Admin',
    ], function () {
        CRUD::resource('user', 'UserCrudController');
    });
