<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\PrinterPrint;

class RetryPrint implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var App\Models\PrinterPrint
     */
    private $print;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(PrinterPrint $print)
    {
        $this->print = $print;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->print->pending = true;
        $this->print->save();
    }
}
