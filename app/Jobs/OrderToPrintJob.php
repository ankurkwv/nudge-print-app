<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Http\Request;

use App\Models\PrinterPrint;
use App\Models\Order;

use Log;
use Exception;
use Storage;

use Carbon\Carbon;

class OrderToPrintJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var App\Models\Order
     */
    private $order;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $order = $this->order;
        $restaurant = $order->restaurant;

        $fileName = 'order_'. $order->id .'_print.xml';

        $order_carbon = new Carbon(
            $order->payload['OrderDateString'] . $order->payload['OrderTimeString'],
            'America/New_York'
        );

        $pickup_carbon = new Carbon(
            $order->payload['PickupDateString'] . $order->payload['PickupTimeString'],
            'America/New_York'
        );

        $data = (Object) [
            'restaurant'    => $restaurant->name,
            'order_id'      => $order->foreign_id,
            'order_time'    => $order_carbon->format('g:ia T'),
            'pickup_time'   => $pickup_carbon->format('g:ia T'),
            'runner'        => $order->payload['Runner'],
            'pickup_date'   => $pickup_carbon->format('m/d/Y'),
            'orders'        => $order->payload['OrderUsers'],
            'subtotal'      => $order->payload['PreTaxTotalDollars'],
            'tax'           => $order->payload['SalesTaxDollars'],
            'total'         => $order->payload['RestaurantTotal']
        ];

        $xmlContent = view('xml.print', compact('data'))->render();
        Storage::put('/prints/'.$fileName, $xmlContent);

        foreach ($restaurant->printers as $printer) {
            
            $this->createPrint($restaurant, $printer, $fileName);  

            if ($printer->double_print) {
                $this->createPrint($restaurant, $printer, $fileName); 
            }       

        }
    }

    private function createPrint($restaurant, $printer, $fileName) {
        $print = new PrinterPrint;
        $print->restaurant_id = $restaurant->id;
        $print->printer_id = $printer->id;
        $print->xml_path = '/prints/'.$fileName;
        $print->pending = true;
        $print->save();     
    }
}
