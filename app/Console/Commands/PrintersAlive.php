<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Printer;
use Carbon\Carbon;

use Log;
use Mail;
use App\Mail\PrinterOffline;

class PrintersAlive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'printers:alive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check printers last activity to determine health status.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $offlinePrinters = Printer::where('status', '!=', 'Offline')
            ->where('last_status_update', '<', Carbon::now()->subMinutes(1));
            
        if($offlinePrinters->count() > 0) {

            foreach($offlinePrinters->get() as $printer) {
                if ($printer->last_error_email < Carbon::now()->subHours(1)) {
                    $users = \App\User::where('notifiable', true)->get();
                    Mail::to($users)->send(new PrinterOffline($printer, "Printer is offline!!"));
                }
            }

            $offlinePrinters->update(['status' => 'Offline']);
        }

    }
}
