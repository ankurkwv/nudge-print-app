<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Events\OnPrinterOffline;

use Mail;
use App\Mail\PrinterOffline;

use Carbon\Carbon; 

class SendPrinterStatusNotification implements ShouldQueue
{
    public $tries = 1;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\OnPrinterOffline  $event
     * @return void
     */
    public function handle(OnPrinterOffline $event)
    {

        \Log::info("LISTENING");
        if ($event->printer->last_error_email < Carbon::now()->subHours(1)) {
        \Log::info("SENDING ERROR EMAIL");
        $users = \App\User::where('notifiable', true)->get();
        Mail::to($users)->send(new PrinterOffline($event->printer, $event->message));    
        }

    }
}
