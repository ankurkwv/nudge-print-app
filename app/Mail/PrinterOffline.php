<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Repositories\SlackRepository;

class PrinterOffline extends Mailable
{
    use Queueable, SerializesModels;

    public $printer;
    public $message;

    private $slack;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($printer, $message)
    {
        $this->printer = $printer;
        $this->message = $message;
        $this->slack = new SlackRepository;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->printer->last_error_email = date('Y-m-d H:i:s');
        $this->printer->save();

        $this->slack->send($this->printer->printer_friendly . " " . $this->message);
        return $this->markdown('emails.printers.offline');
    }
}
