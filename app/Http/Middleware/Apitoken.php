<?php

namespace App\Http\Middleware;

use Closure;
use Log;

class Apitoken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->api_token !== env('API_KEY')) {
            Log::info($request->api_token);
            Log::info(env('API_KEY'));
            return response()->json('Unauthorized', 401);
        } 

        return $next($request);
    }
}
