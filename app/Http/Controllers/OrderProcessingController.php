<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\OrderToPrintJob;
use App\Jobs\EmailOrder;

use App\Models\Order;
use App\Models\Restaurant;

use Validator;
use Exception;
use Log;

class OrderProcessingController extends Controller
{
    private $request;
    private $restaurant;

    public function index(Request $request)
    {

        $this->request = $request;

        try {
            $this->validateRequest();
        }
        catch (Exception $e) {
            return response()->json($e->getMessage(), 500);
        }

        if ($this->request->input('env') == "stage") {
            $stagingOrder = true;
        } else {
            $stagingOrder = false;
        }

        $this->restaurant = Restaurant::where('foreign_loc_id', $this->request->input('LocationId'))
            ->where('staging', $stagingOrder)
            ->firstOrFail();

        $order = $this->saveAndReturnOrder();

    	EmailOrder::dispatch($order);
        OrderToPrintJob::dispatch($order);

		return response()->json('Processed', 200);
    }

    private function validateRequest() {

        $validator = Validator::make($this->request->all(), [
            'OrderId' => 'required|integer',
            'LocationId' => 'required|integer',
            'LocationName' => 'required|string',
            'OrderDateString' => 'required|string',
            'OrderTimeString' => 'required|string',
            'PickupDateString' => 'required|string',
            'PickupTimeString' => 'required|string',
            'Runner' => 'required|string',
            'PreTaxTotalDollars' => 'required|numeric',
            'SalesTaxDollars' => 'required|numeric',
            'RestaurantTotal' => 'required|numeric',
            'OrderUsers' => 'required',
            'api_token' => 'required',
            'env' => 'string'
        ]);

        if ($validator->fails()) {
            throw new Exception('Form validation failed.' . $validator->messages());
        }

        // $this->checkForDuplicate();

        return;
    }

    private function checkForDuplicate() {        

        $previousOrder = Order::where('foreign_id', $this->request->input('OrderId'))->first();

        if ($previousOrder) {
            Log::info('Nudge sent preexisting order.');
            Log::info('Details --------------------');
            Log::info('Local ID:    ' . $previousOrder->id);
            Log::info('Foreign ID:  ' . $this->request->input('OrderId'));
            Log::info('----------------------------');
            throw new Exception('Order already exists!');
        }

        return;
    }

    private function saveAndReturnOrder() {

        $payload = [
            'LocationName'          => $this->request->input('LocationName'),
            'OrderId'               => $this->request->input('OrderId'),
            'LocationId'            => $this->request->input('LocationId'),
            'OrderDateString'       => $this->request->input('OrderDateString'),
            'OrderTimeString'       => $this->request->input('OrderTimeString'),
            'PickupDateString'      => $this->request->input('PickupDateString'),
            'PickupTimeString'      => $this->request->input('PickupTimeString'),
            'Runner'                => $this->request->input('Runner'),
            'PreTaxTotalDollars'    => $this->request->input('PreTaxTotalDollars'),
            'SalesTaxDollars'       => $this->request->input('SalesTaxDollars'),
            'RestaurantTotal'       => $this->request->input('RestaurantTotal'),
            'OrderUsers'            => $this->request->input('OrderUsers')
        ];

        $order = new Order;
        $order->restaurant_id = $this->restaurant->id;
        $order->payload = $payload;
        $order->foreign_id = $this->request->input('OrderId');
        $order->save();

        return $order;
    }
}
