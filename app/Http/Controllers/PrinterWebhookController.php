<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\PrinterRepository;

use App\Models\Restaurant;
use App\Models\Printer;
use App\Models\PrinterPrint;

use Log;

class PrinterWebhookController extends Controller {

	/**
	 ** SimpleXMLElement
	 */

	private $xml;

	/**
	 ** App/Models/Restaurant
	 */

	private $restaurant;

	/**
	 ** App/Models/Printer
	 */

	private $printer;


	/**
	 ** App\Repositories\PrinterRepository
	 **/

	private $printerRepository;

	/**
	 ** Construct the controller.
	 */

	public function __construct() {

		$this->printerRepository = new PrinterRepository;
	}

	/**
	 ** Determine the webhook type and handle it.
	 **
	 ** @param Request $request
	 ** @return mixed
	 */

	public function handleWebhook(Request $request) {

		// \Log::info($request->ConnectionType);

		$requestType = $request->input('ConnectionType');

		try { 
			$this->validateRequest($request, $requestType);
		}
		catch (\Exception $e) {
			\Log::error($e->getMessage());
			return;
		}

		if($requestType == 'SetStatus') { // Status update
			// Log::info($request->all());
			$this->printerRepository->updateStatusFromXML($this->xml, $this->printer);
		}
		else if($requestType == 'GetRequest') { // Retrieve pending prints
			
			$pendingPrintsXML = $this->printerRepository->getPrinterPendingPrintsAsXML($this->printer);

			return response($pendingPrintsXML)->withHeaders([
				'Content-Type' => 'text/xml'
			]);
		}
		else if($requestType == 'SetResponse') { // Update completed print
			// Log::info($request->all());
			$this->printerRepository->updatePrintsFromXML($this->xml);
		}

		return Response(null, 200);
	}

	/**
	 ** Validate request and setup the controller variables
	 **
	 ** @param Request $request
	 ** @return void
	 **/

	protected function validateRequest(Request $request, $type) {

		if ($type === "SetStatus") {
			$this->xml = simplexml_load_string($request->input('Status'));
		}
		else {
			$this->xml = simplexml_load_string($request->input('ResponseFile'));
		}

		if( ! $this->xml && $type !== "GetRequest") {
    		throw new \Exception('Invalid XML');
    	}

		// Find printer by ID
		$this->printer = Printer::where('printer_id', $request->input('Name'))->first();

		if( ! $this->printer) {
			// \Log::info('Printer not found');
    		throw new \Exception();
		}

		$this->restaurant = $this->printer->restaurant;
	}
}