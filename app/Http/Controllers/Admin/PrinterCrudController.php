<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PrinterRequest as StoreRequest;
use App\Http\Requests\PrinterRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class PrinterCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class PrinterCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Printer');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/printers');
        $this->crud->setEntityNameStrings('printer', 'printers');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */


        $this->crud->addColumn([
            'name' => 'printer_friendly',
            'label' => 'Friendly Name',
            'type' => 'text'
        ]);

        $this->crud->addColumn([
            'name' => 'printer_id',
            'label' => 'Machine Name',
            'type' => 'text'
        ]);

        $this->crud->addColumn([
            'name' => 'status',
            'label' => 'Status',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'printer_friendly',
            'label' => 'Friendly Name',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'printer_id',
            'label' => 'Machine Name',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'double_print',
            'label' => 'Double Print?',
            'type' => 'checkbox'
        ]);

        $this->crud->addField([
           'label'     => "Restaurants",
           'type'      => 'select2_multiple',
           'name'      => 'restaurants',
           'entity'    => 'restaurants',
           'attribute' => 'name',
           'model'     => "App\Models\Restaurant",
           'pivot'     => true,
           'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            })
        ]);


        // add asterisk for fields that are required in PrinterRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
