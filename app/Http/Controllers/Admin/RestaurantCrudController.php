<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\RestaurantRequest as StoreRequest;
use App\Http\Requests\RestaurantRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class RestaurantCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class RestaurantCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Restaurant');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/restaurants');
        $this->crud->setEntityNameStrings('restaurant', 'restaurants');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->addColumn([
            'label'     => 'Restaurant',
            'name'      => 'name'
        ]);

        $this->crud->addColumn([
            'label'     => 'Staging?',
            'name'      => 'staging',
            'type'      => 'boolean'
        ]);

        $this->crud->addField([
            'label'     => 'Restaurant',
            'name'      => 'name',
            'type'      => 'text'
        ]);

        $this->crud->addField([
            'label'     => 'Foreign ID',
            'name'      => 'foreign_loc_id',
            'type'      => 'text'
        ]);

        $this->crud->addField([
            'label'     => 'Staging?',
            'name'      => 'staging',
            'type'      => 'checkbox'
        ]);

        // add asterisk for fields that are required in RestaurantRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
