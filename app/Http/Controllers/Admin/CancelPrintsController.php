<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\PrinterPrint;
use Log;

class CancelPrintsController extends Controller
{

/**
 *
 * Validates that a particular print is 
 * pending, and sets pending to false
 *
 * Returns JSON
 *
 */

    public function cancel(Request $request)
    {
        $print = PrinterPrint::findOrFail($request->print);

        if ($print->pending) {
            $print->pending = false;
            $print->save();
            return response('Success!', 200);
        }

        return response('Fail!', 400);
    }

}
