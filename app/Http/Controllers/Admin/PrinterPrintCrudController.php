<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
    

use Backpack\CRUD\CrudPanel;

/**
 * Class PrinterPrintCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class PrinterPrintCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\PrinterPrint');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/prints');
        $this->crud->setEntityNameStrings('print', 'prints');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        

        $this->crud->addColumn([
           'name' => "created_at", // The db column name
           'label' => "Created At", // Table column heading
           'type' => "better-datetime",
           'format' => 'L LT'
        ]);

        $this->crud->addColumn([
            'name' => 'restaurant_id',
            'label' => 'Restaurant',
            'type' => 'select',
            'entity' => 'restaurant',
            'attribute' => 'name',
            'model' => 'App\Models\Restaurant'
        ]);

        $this->crud->addColumn([
            'name' => 'printer_id',
            'label' => 'Printer',
            'type' => 'select',
            'entity' => 'printer',
            'attribute' => 'printer_friendly',
            'model' => 'App\Models\Printer'
        ]);

        $this->crud->addColumn([
            'name' => 'pending',
            'label' => 'Pending',
            'type' => 'boolean'
        ]);

        $this->crud->addColumn([
            'name' => 'error',
            'label' => 'Error',
            'type' => 'text'
        ]);

        $this->crud->addColumn([
            'name' => 'attempts',
            'label' => 'Printing Attempts',
            'type' => 'text'
        ]);

        $this->crud->allowAccess('list', 'delete');
        $this->crud->denyAccess(['create']);

    $this->crud->removeAllButtons();

        if (!$this->request->has('order')) {
            $this->crud->orderBy('created_at', 'DESC');
        }

        $this->crud->addButtonFromView('line', 'cancel-pending-order', 'cancel-pending-order', 'end');
        $this->crud->addButtonFromView('line', 'resend-order', 'resend-order', 'end');
    }
}
