<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use App\Models\Printer;
use App\Models\PrinterPrint;

class DashboardController extends Controller
{
    public function index() {
        $chartData = $this->getChartData();

        $printers = Printer::all()->count();
        $printersWithErrors = Printer::where('status', 'Error')
            ->orWhere('status', 'Offline')
            ->count();
        $printsSent = PrinterPrint::all()->count();
        $printsWithErrors = PrinterPrint::where('error', '!=', null)
            ->count();

        $widgets = [
            'printers' => [
                'all' => $printers,
                'errors' => $printersWithErrors,
            ],
            'prints' => [
                'all' => $printsSent,
            ]
        ];

        return view('vendor.backpack.base.dashboard', compact('chartData', 'widgets'));
    }

    private static function getChartData() {
        $carbon = Carbon::now();
        $chart = [
            'labels' => [], 
            'data' => [],
            'time' => [
                'start' => Carbon::now()->startOfYear()->toFormattedDateString(),
                'end' => Carbon::now()->toFormattedDateString()
            ]
        ];

        // Setup labels (January to current month)
        for($i = 1; $i != $carbon->month + 1; $i++) {
            $chart['labels'][$i] = date('F', mktime(0, 0, 0, $i, 10));
            $chart['data'][$i] = 0;
        }

        // Setup chart data
        $prints = []; // model is missing

        foreach($prints as $print) {
            $month = $print->created_at->month;
            if(isset($chart['data'][$month])) {
                $chart['data'][$month] ++;
            }
            else $chart['data'][$month] = 1;
        }
        return $chart;
    }
}
