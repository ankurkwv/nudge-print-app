<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\PrinterPrint;
use Log;

class ResendPrintsController extends Controller
{

/**
 *
 * Validates that a particular print is 
 * pending, and sets pending to false
 *
 * Returns JSON
 *
 */

    public function resend(Request $request)
    {
        $print = PrinterPrint::findOrFail($request->print);

        if (!$print->pending) {
            $print->pending = true;
            $print->save();
            return response('Success!', 200);
        }

        return response('Fail!', 400);
    }

}
