<?php

namespace App\Repositories;

use Log;

class SlackRepository {

  /**
   *
   * Send message to slack from 
   * Slack configured incoming
   * webhook URL
   *
   */
  
	public function send($message) {
		
    $client = new \GuzzleHttp\Client();
    $url = env('SLACK_WEBHOOK_URL');
   
    $response = $client->post($url, [
        \GuzzleHttp\RequestOptions::JSON => ['text' => $message]
    ]);

  }
}