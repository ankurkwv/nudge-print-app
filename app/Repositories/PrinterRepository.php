<?php

namespace App\Repositories;

use App\Models\Printer;
use App\Models\PrinterPrint;

use App\Jobs\RetryPrint;

use Log;
use SimpleXMLElement;

class PrinterRepository {

	/**
	 ** Update printer status with data obtained from the XML File
	 **
	 ** @return null
	 */

	public function updateStatusFromXML(\SimpleXMLElement $xml, Printer $printer) {
		
		foreach ($xml->printerstatus as $printerstatus) {

			$statusMessage = $this->getStatusMessageByCode(
        $printer, 
        $printerstatus['asbstatus']
      );
      
			$printer->status = $statusMessage;
			$printer->last_status_update = date('Y-m-d H:i:s');
			$printer->save();

		}

  }

  /**
   ** Get all pending prints for a specific printer in XML format
	 ** 
	 ** @return null
   */

  public function getPrinterPendingPrintsAsXML(Printer $printer) {

	$printsQuery = $printer->prints()->where('pending', 1);

    if ($printsQuery->count() > 0) {
        $prints = $printsQuery->get();

        // Create the XML
        $xml = view('xml.prints', compact('prints'))->render();

        Log::info('Sending Print!!!!');

        // Set prints as no longer pending (we sent them)
        $printsQuery->update(['pending' => false]);
        return $xml;
    }

    return null; 

  }

  /**
   ** Update the Print in our database after the Printer updates us about it. 
   **
   ** @return null
   **/

  public function updatePrintsFromXML(SimpleXMLElement $xml) {

  	if( ! isset($xml->ePOSPrint)) {

  		// This normally occurs when there is a problem in the XML file.
  		return null;
  	}

  	foreach ($xml->ePOSPrint as $eposprint) {

  		// Find the print ID in our database
  		if( ! isset($eposprint->Parameter->printjobid)) {
  			\Log::info('updatePrintsFromXML: `printjobid` not found');
  			continue;
  		}

          $print = PrinterPrint::find($eposprint->Parameter->printjobid);

          if( ! $print) {
          	\Log::info('updatePrintsFromXML: `Print Model` not found');
  			continue;
  		}

  		// Handle the new information
  		$data = $eposprint->PrintResponse->response;
      Log::info(print_r($data, true));
      if($data['success'] == "false") {
        Log::error('Printer error!!!!');
      	
    		$error = $data['code'];
    		$print->error = $error;
        $print->attempts = $print->attempts + 1;
    		$print->save();

        if ($print->attempts < 10) {
          RetryPrint::dispatch($print)->delay(now()->addMinutes(5));
        }

      } else {
        $print->attempts = $print->attempts + 1;
      	$print->error = null;
      	$print->save();
      }

    }

  }

    /**
     ** Get status description by code.
     **
     ** @return string
     **/

    protected function getStatusMessageByCode($printer, $statusCode) {
        // Log::info($printer->printer_friendly . ': ' . $statusCode);
        if ($statusCode != "0x0F000014") {
            Log::error($printer->printer_friendly . "---- OFFLINE!");
            event(new \App\Events\OnPrinterOffline($printer, "The following printer has some error. Please check the printer."));
            return "Offline";
        }
        return "Online";
    }
}