<?php

namespace App\Repositories;

use Log;

class SendgridRepository {

  /**
   *
   * Send order through Sendgrid to 
   * help@nudgeordering.com
   *
   */
  
	public function emailOrder($order) {
		$email = new \SendGrid\Mail\Mail();
    $email->setFrom("support@nudgeordering.com", "Nudge");
    $email->setSubject("New Nudge Order");
    $email->addTo(
            getenv('SENDGRID_TO'),
            "Nudge Help Line",
            $order->payload,
            0
        );
    $email->setTemplateId("d-6d49526fd2b6449e8c079d485d759df3");
    $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
    try {
        $response = $sendgrid->send($email);
        Log::info("Sendgrid response code: " . $response->statusCode());
    } catch (Exception $e) {
        Log::error('Caught exception: '.  $e->getMessage(). "\n");
    }

  }
}