<epos-print xmlns="http://www.epson-pos.com/schemas/2011/03/epos-print">
<text lang="en"/>
<text smooth="true"/>
<text align="center"/>
<text width="2" height="2"/>
<text reverse="true" ul="false" em="true" color="color_1"/>
<text>NUDGE ORDER</text>
<feed unit="75"/>
<text smooth="true"/>
<text align="center"/>
<text width="2" height="2"/>
<text reverse="false" ul="false" em="true" color="color_1"/>
<text>{{ wordwrap($data->restaurant, 20) }}</text>
<feed unit="100"/>
<text align="left"/>
<text width="1" height="1"/>
<text reverse="false" ul="false" em="true" color="color_1"/>
<text>Order: {{$data->order_id}}</text>
<feed unit="40"/>
<text>Order Placed: {{$data->order_time}}</text>
<feed unit="40"/>
<text>Pickup Time: {{$data->pickup_time}}</text>
<feed unit="40"/>
<text>Nudge Help: (850) 222-5793</text>
<feed unit="80"/>
<text reverse="false" ul="false" em="false" color="color_1"/>
<text>Person picking up: {{$data->runner}}</text>
<feed unit="40"/>
<text>Pickup {{$data->pickup_date}}, at {{$data->pickup_time}}</text>
@foreach ($data->orders as $order)
<feed unit="80"/>
<text font="font_a"/>
<text>{{ $order['Name'] }}'s Order</text>
@foreach ($order['Items'] as $item)
<text font="font_a"/>
<feed unit="40"/>
<text>[{{ $item['Quantity'] }}] {{ wordwrap($item['ItemName'], 30) }}</text>
<text x="420"/>
<text>${{ $item['RestaurantItemPriceDollars'] }}</text>
@if(isset($item['PriceLevelName']))
<feed unit="40"/>
<text font="font_b"/>
<text x="40"/>
<text>Type: {{ $item['PriceLevelName'] }}</text>
@endif
@if (!empty($item['OrderItemModifiers']) && $item['OrderItemModifiers'] !== null)
@foreach ($item['OrderItemModifiers'] as $modifier)
<feed unit="40"/>
<text font="font_b"/>
<text x="40"/>
<text>{{ $modifier['ModifierGroupName'] }} {{ $modifier['ModifierName'] }}</text>
@endforeach
@endif
@endforeach
@endforeach
<text font="font_a"/>
<feed unit="100"/>
<text align="right"/>
<text>PREPAID DO NOT CHARGE</text>
<feed unit="40"/>
<text align="right"/>
<text>SUBTOTAL: ${{ $data->subtotal }}</text>
<feed unit="40"/>
<text align="right"/>
<text>TAX: ${{ $data->tax }}</text>
<feed unit="40"/>
<text align="right"/>
<text>TOTAL: ${{ $data->total }}</text>
<feed unit="100"/>
<cut type="feed"/>
</epos-print>