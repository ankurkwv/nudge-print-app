<?xml version="1.0" encoding="utf-8" ?>
<PrintRequestInfo Version="3.00">
    @foreach($prints as $print)
    <ePOSPrint>
        <Parameter>
            <devid>local_printer</devid>
            <timeout>10000</timeout>
            <printjobid>{{$print->id}}</printjobid>
        </Parameter>
        <PrintData>
            {!! $print->getRawXML() !!}
        </PrintData>
    </ePOSPrint>
    @endforeach
</PrintRequestInfo>