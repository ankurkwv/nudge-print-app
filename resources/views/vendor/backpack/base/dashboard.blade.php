@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
        {{ trans('backpack::base.dashboard') }}
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">{{ trans('backpack::base.dashboard') }}</li>
      </ol>
    </section>
@endsection


@section('content')
    <div class="row">
      <div class="col-sm-4">
        <div class="info-box">
          <span class="info-box-icon bg-aqua"><i class="fa fa-print"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">PRINTERS CONNECTED</span>
            <span class="info-box-number">{{$widgets['printers']['all']}}</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <div class="col-sm-4">
        <div class="info-box">
          <span class="info-box-icon bg-red"><i class="fa fa-print"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">PRINTERS WITH ERROR</span>
            <span class="info-box-number">{{$widgets['printers']['errors']}}</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>

      <div class="col-sm-4">
        <div class="info-box">
          <span class="info-box-icon bg-aqua"><i class="fa fa-sticky-note-o"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">PRINTS SENT</span>
            <span class="info-box-number">{{$widgets['prints']['all']}}</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Monthly Prints Report</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <p class="text-center">
                    <strong>{{$chartData['time']['start']}} - {{$chartData['time']['end']}}</strong>
                  </p>

                  <div class="chart">
                    <!-- Sales Chart Canvas -->
                    <canvas id="salesChart" style="height: 180px;"></canvas>
                  </div>
                  <!-- /.chart-responsive -->
                </div>
              </div>
            </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
@endsection

@push('after_scripts')

<script>
$(function () {

'use strict';

/* ChartJS
 * -------
 * Here we will create a few charts using ChartJS
 */

// -----------------------
// - MONTHLY SALES CHART -
// -----------------------

// Get context with jQuery - using jQuery's .get() method.
var salesChartCanvas = $('#salesChart').get(0).getContext('2d');
// This will get the first returned node in the jQuery collection.
var salesChart       = new Chart(salesChartCanvas);

var salesChartData = {
  labels  : ["{!! implode('","', $chartData['labels']) !!}"],
  datasets: [
    {
      label               : 'Prints',
      fillColor           : 'rgba(60,141,188,0.9)',
      strokeColor         : 'rgba(60,141,188,0.8)',
      pointColor          : '#3b8bba',
      pointStrokeColor    : 'rgba(60,141,188,1)',
      pointHighlightFill  : '#fff',
      pointHighlightStroke: 'rgba(60,141,188,1)',
      data                : [{!! implode(',', $chartData['data'])!!}]
    }
  ]
};

var salesChartOptions = {
  // Boolean - If we should show the scale at all
  showScale               : true,
  // Boolean - Whether grid lines are shown across the chart
  scaleShowGridLines      : false,
  // String - Colour of the grid lines
  scaleGridLineColor      : 'rgba(0,0,0,.05)',
  // Number - Width of the grid lines
  scaleGridLineWidth      : 1,
  // Boolean - Whether to show horizontal lines (except X axis)
  scaleShowHorizontalLines: true,
  // Boolean - Whether to show vertical lines (except Y axis)
  scaleShowVerticalLines  : true,
  // Boolean - Whether the line is curved between points
  bezierCurve             : true,
  // Number - Tension of the bezier curve between points
  bezierCurveTension      : 0.3,
  // Boolean - Whether to show a dot for each point
  pointDot                : false,
  // Number - Radius of each point dot in pixels
  pointDotRadius          : 4,
  // Number - Pixel width of point dot stroke
  pointDotStrokeWidth     : 1,
  // Number - amount extra to add to the radius to cater for hit detection outside the drawn point
  pointHitDetectionRadius : 20,
  // Boolean - Whether to show a stroke for datasets
  datasetStroke           : true,
  // Number - Pixel width of dataset stroke
  datasetStrokeWidth      : 2,
  // Boolean - Whether to fill the dataset with a color
  datasetFill             : true,
  // String - A legend template
  legendTemplate          : '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<datasets.length; i++){%><li><span style=\'background-color:<%=datasets[i].lineColor%>\'></span><%=datasets[i].label%></li><%}%></ul>',
  // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
  maintainAspectRatio     : true,
  // Boolean - whether to make the chart responsive to window resizing
  responsive              : true
};

// Create the line chart
salesChart.Line(salesChartData, salesChartOptions);

// ---------------------------
// - END MONTHLY SALES CHART -
// ---------------------------
});
</script>
@endpush