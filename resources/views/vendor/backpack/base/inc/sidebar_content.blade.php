<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li><a href="{{ backpack_url('printers') }}"><i class="fa fa-print"></i> <span>Printers</span></a></li>
<li><a href="{{ backpack_url('restaurants') }}"><i class="fa fa-cutlery"></i> <span>Restaurants</span></a></li>
<li><a href="{{ backpack_url('prints') }}"><i class="fa fa-sticky-note-o"></i> <span>Prints</span></a></li>
<li><a href="{{ backpack_url('user') }}"><i class="fa fa-user"></i> <span>Users</span></a></li>
