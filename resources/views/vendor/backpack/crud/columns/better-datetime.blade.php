{{-- localized datetime using carbon --}}
@php
    $value = data_get($entry, $column['name']);
@endphp

<span data-order="{{ $value }}">
    @if (!empty($value))
	{{
		\Carbon\Carbon::parse($value)
		->locale(App::getLocale())
		->tz('America/New_York')
		->isoFormat($column['format'] ?? config('backpack.base.default_datetime_format'))
	}} EST
    @endif
</span>