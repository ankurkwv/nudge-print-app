@if (!$entry->pending)
	@if ($crud->hasAccess('delete'))
		<a href="javascript:void(0)" onclick="resendEntry(this)" data-route="{{ url($crud->route.'/'.$entry->getKey()) }}/resend" class="btn btn-xs btn-default" data-button-type="resend"><i class="fa fa-send"></i> Resend</a>
	@endif
@endif

<script>
	if (typeof resendEntry != 'function') {
	  $("[data-button-type=resend]").unbind('click');

	  function resendEntry(button) {

	      var button = $(button);
	      var route = button.attr('data-route');
	      var row = $("#crudTable a[data-route='"+route+"']").closest('tr');

	      if (confirm("Are you sure you want to resend this print?") == true) {
	          $.ajax({
	              url: route,
	              type: 'GET',
	              success: function(result) {
	                  // Show an alert with the result
	                  new PNotify({
	                      title: "Print Resend",
	                      text: "The print was set to pending and will reprint.",
	                      type: "success"
	                  });

	                  // Hide the modal, if any
	                  $('.modal').modal('hide');

	                  // Remove the details row, if it is open
	                  if (row.hasClass("shown")) {
	                      row.next().remove();
	                  }
	              },
	              error: function(result) {
	                  // Show an alert with the result
	                  new PNotify({
	                      title: "Error",
	                      text: "Sorry, there was an error with setting this status. ",
	                      type: "warning"
	                  });
	              }
	          });
	      } else {
	      	  // Show an alert telling the user we don't know what went wrong
	          new PNotify({
	              title: "Error",
	              text: "Sorry, there was an error with setting this status. ",
	              type: "info"
	          });
	      }
      }
	}

	// make it so that the function above is run after each DataTable draw event
	// crud.addFunctionToDataTablesDrawEventQueue('unpendEntry');
</script>