@if ($entry->pending)
	@if ($crud->hasAccess('delete'))
		<a href="javascript:void(0)" onclick="unpendEntry(this)" data-route="{{ url($crud->route.'/'.$entry->getKey()) }}/cancel" class="btn btn-xs btn-default" data-button-type="unpend"><i class="fa fa-trash"></i> Cancel Pending</a>
	@endif
@endif

<script>
	if (typeof unpendEntry != 'function') {
	  $("[data-button-type=unpent]").unbind('click');

	  function unpendEntry(button) {

	      var button = $(button);
	      var route = button.attr('data-route');
	      var row = $("#crudTable a[data-route='"+route+"']").closest('tr');

	      if (confirm("Are you sure you want to cancel this print?") == true) {
	          $.ajax({
	              url: route,
	              type: 'GET',
	              success: function(result) {
	                  // Show an alert with the result
	                  new PNotify({
	                      title: "Print Cancellation",
	                      text: "The prints status was set to not pending. It will not print now.",
	                      type: "success"
	                  });

	                  // Hide the modal, if any
	                  $('.modal').modal('hide');

	                  // Remove the details row, if it is open
	                  if (row.hasClass("shown")) {
	                      row.next().remove();
	                  }
	              },
	              error: function(result) {
	                  // Show an alert with the result
	                  new PNotify({
	                      title: "Error",
	                      text: "Sorry, there was an error with setting this status. ",
	                      type: "warning"
	                  });
	              }
	          });
	      } else {
	      	  // Show an alert telling the user we don't know what went wrong
	          new PNotify({
	              title: "Error",
	              text: "Sorry, there was an error with setting this status. ",
	              type: "info"
	          });
	      }
      }
	}

	// make it so that the function above is run after each DataTable draw event
	// crud.addFunctionToDataTablesDrawEventQueue('unpendEntry');
</script>