<?php
date_default_timezone_set("America/New_York");
?>
@component('mail::message')
# Printer Error

{{ $message }}

Time: {{ date("h:ia T") }}

Printer: {{ $printer->printer_friendly }}

Printer Restaurants: {{ $printer->restaurants()->pluck('name') }}
@endcomponent
