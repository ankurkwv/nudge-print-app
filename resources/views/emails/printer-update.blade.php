@component('mail::message')
@if($printer->status == 'Offline')
# Offline Printer

It has been a while since we received any signal from the printer.<br/><br/>

@else
# Printer Error

A printer is outputting the following error: <strong>{{$printer->error}}</strong>. What could be happening?
@endif

<strong>Printer:</strong> <span>{{$printer->printer_friendly}}</span> <span>{{$printer->printer_id}}</span>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
